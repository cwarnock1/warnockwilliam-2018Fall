//
//  UpcomingViewController.swift
//  Pill Bottle
//
//  Created by Christopher Warnock on 12/2/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import UserNotifications

class MyTableViewCell2: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var subtitle: UILabel!
}

class UpcomingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var data: [String] = []
    var dictionary: [String:[String]] = [:]
    @IBOutlet weak var countLabel: UILabel!
    var notifications: [UNNotificationRequest] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests(completionHandler: { requests in
            self.notifications = requests
//            for request in requests {
//                //print(request)
//            }
            DispatchQueue.main.async {
                self.countLabel.text = "You currently have \(self.notifications.count) set notifications"
            }
        })
        
        fillLabel()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests(completionHandler: { requests in
            self.notifications = requests
//            for request in requests {
//                //print(request)
//            }
            DispatchQueue.main.async {
                self.countLabel.text = "You currently have \(self.notifications.count) set notifications"
            }
        })
        
        fillLabel()
        self.tableView.reloadData()
    }
    

    func fillLabel() {
        var nameDateDictionary: [String:[String]] = [:]
        data = []
        
        if notifications != [] {
            var dayArray: [String] = []
            var currentName: String = ""
            
            for i in 0...notifications.count - 1 {
//                print("\(notifications[i].identifier)")
                let nameArray = notifications[i].identifier.components(separatedBy: ".")
                
                if currentName == nameArray[0] {
                    dayArray.append(nameArray[1])
                    nameDateDictionary.updateValue(dayArray, forKey: nameArray[0])
                }else {
                    currentName = nameArray[0]
                    dayArray = []
                    if !data.contains(nameArray[0]){
                        data.append(nameArray[0])
                    }
                    
                    dayArray.append(nameArray[1])
                    nameDateDictionary.updateValue(dayArray, forKey: nameArray[0])
                }
                
            }
//            print(nameDateDictionary)
            dictionary = nameDateDictionary
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "reusableCell2") as! MyTableViewCell2
        
        let text = data[indexPath.row]
        cell.label?.text = text
        
        let days = dictionary[text]
        var subtitleString = ""
        
        for i in 0...days!.count - 1 {
            let test = Int(days![i])
            switch test {
            case 1:
                subtitleString.append(contentsOf: "Sunday, ")
            case 2:
                subtitleString.append(contentsOf: "Monday, ")
            case 3:
                subtitleString.append(contentsOf: "Tuesday, ")
            case 4:
                subtitleString.append(contentsOf: "Wednesday, ")
            case 5:
                subtitleString.append(contentsOf: "Thursday, ")
            case 6:
                subtitleString.append(contentsOf: "Friday, ")
            case 7:
                subtitleString.append(contentsOf: "Saturday")
            default:
                print("Strange")
            }
        }
        cell.subtitle?.text = subtitleString
        
        return cell
    }

}
