//
//  CustomButton.swift
//  Project 2
//
//  Created by Christopher Warnock on 11/23/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let buttonColor = UIColor.init(named: "backgroundGreen")
        let textColor = UIColor.black
        
        let bthWidth = 200
        let btnHeight = 60
        
        self.frame.size = CGSize(width: bthWidth, height: btnHeight)
        self.frame.origin = CGPoint(x: (((superview?.frame.width)! / 2) - (self.frame.width / 2)), y: self.frame.origin.y)
        
        self.layer.cornerRadius = 10.0
        self.backgroundColor = buttonColor
        self.setTitleColor(textColor, for: .normal)
        self.clipsToBounds = true
        
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.setTitle(self.titleLabel?.text?.capitalized, for: .normal)
        
    }

}
