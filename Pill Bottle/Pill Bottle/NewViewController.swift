//
//  NewViewController.swift
//  Pill Bottle
//
//  Created by Christopher Warnock on 11/30/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

extension NewViewController : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}

class NewViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var medicationName: UITextField!
    @IBOutlet weak var instructions: UITextView!
    @IBOutlet weak var timePicker: UIDatePicker!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UNUserNotificationCenter.current().delegate = self
    }
    
    @IBAction func addImageTapped(_ sender: Any) {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image2 = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image.image = image2
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if image.image != nil && medicationName.text != "" && instructions.text != "" {
            createDatabaseItem()
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.dateFormat = "HH:mm:ss"
            
            let strTime = formatter.string(from: timePicker.date)
            var weekdays: [Int] = []
            
            if !sunBool {
                weekdays.append(1)
            }
            if !monBool {
                weekdays.append(2)
            }
            if !tuesBool {
                weekdays.append(3)
            }
            if !wedBool {
                weekdays.append(4)
            }
            if !thurBool {
                weekdays.append(5)
            }
            if !friBool {
                weekdays.append(6)
            }
            if !satBool {
                weekdays.append(7)
            }
            
            
            createNotificatation(medicationName: medicationName.text!, weekdays: weekdays, time: strTime)
            
            let alert = UIAlertController(title: "Medication Added", message: "The medication was added to your list", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }else {
            let alert = UIAlertController(title: "Medication Not Added", message: "Please fill out all information", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        
        image.image = nil
        medicationName.text = ""
        instructions.text = ""
        monBool = true
        mondayButton.backgroundColor = UIColor.white
        tuesBool = true
        tuesdayButton.backgroundColor = UIColor.white
        wedBool = true
        wednesdayButton.backgroundColor = UIColor.white
        thurBool = true
        thursdayButton.backgroundColor = UIColor.white
        friBool = true
        fridayButton.backgroundColor = UIColor.white
        satBool = true
        saturdayButton.backgroundColor = UIColor.white
        sunBool = true
        sundayButton.backgroundColor = UIColor.white
    }
    
    func createDatabaseItem () {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "HH:mm:ss"
        let strTime = formatter.string(from: timePicker.date)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Medication", in: managedContext)!
        let med = NSManagedObject(entity: entity, insertInto: managedContext)
        
        med.setValue(medicationName.text, forKeyPath: "name")
        med.setValue(instructions.text, forKeyPath: "directions")
        med.setValue(NSData(data: image.image!.jpegData(compressionQuality: 0.3)!), forKeyPath: "picture")
        med.setValue(strTime, forKey: "time")
        med.setValue(monBool, forKey: "mon")
        med.setValue(tuesBool, forKey: "tues")
        med.setValue(wedBool, forKey: "wed")
        med.setValue(thurBool, forKey: "thurs")
        med.setValue(friBool, forKey: "fri")
        med.setValue(satBool, forKey: "sat")
        med.setValue(sunBool, forKey: "sun")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //Day Selectors
    @IBOutlet weak var sundayButton: UIButton!
    var sunBool: Bool = true
    @IBAction func sunday(_ sender: Any) {
        if sunBool {
            sundayButton.backgroundColor = UIColor.init(named: "otherGrey")
            sunBool = false
        }else {
            sundayButton.backgroundColor = UIColor.white
            sunBool = true
        }
    }
    
    @IBOutlet weak var mondayButton: UIButton!
    var monBool: Bool = true
    @IBAction func monday(_ sender: Any) {
        if monBool {
            mondayButton.backgroundColor = UIColor.init(named: "otherGrey")
            monBool = false
        }else {
            mondayButton.backgroundColor = UIColor.white
            monBool = true
        }
    }
    
    
    @IBOutlet weak var tuesdayButton: UIButton!
    var tuesBool: Bool = true
    @IBAction func tuesday(_ sender: Any) {
        if tuesBool {
            tuesdayButton.backgroundColor = UIColor.init(named: "otherGrey")
            tuesBool = false
        }else {
            tuesdayButton.backgroundColor = UIColor.white
            tuesBool = true
        }
    }
    
    @IBOutlet weak var wednesdayButton: UIButton!
    var wedBool: Bool = true
    @IBAction func wednesday(_ sender: Any) {
        if wedBool {
            wednesdayButton.backgroundColor = UIColor.init(named: "otherGrey")
            wedBool = false
        }else {
            wednesdayButton.backgroundColor = UIColor.white
            wedBool = true
        }
    }
    
    @IBOutlet weak var thursdayButton: UIButton!
    var thurBool: Bool = true
    @IBAction func thursday(_ sender: Any) {
        if thurBool {
            thursdayButton.backgroundColor = UIColor.init(named: "otherGrey")
            thurBool = false
        }else {
            thursdayButton.backgroundColor = UIColor.white
            thurBool = true
        }
    }
    
    @IBOutlet weak var fridayButton: UIButton!
    var friBool: Bool = true
    @IBAction func friday(_ sender: Any) {
        if friBool {
            fridayButton.backgroundColor = UIColor.init(named: "otherGrey")
            friBool = false
        }else {
            fridayButton.backgroundColor = UIColor.white
            friBool = true
        }
    }
    
    @IBOutlet weak var saturdayButton: UIButton!
    var satBool: Bool = true
    @IBAction func saturday(_ sender: Any) {
        if satBool {
            saturdayButton.backgroundColor = UIColor.init(named: "otherGrey")
            satBool = false
        }else {
            saturdayButton.backgroundColor = UIColor.white
            satBool = true
        }
    }
    
    //Notification
    func createNotificatation(medicationName: String, weekdays: [Int], time: String) {
        let content = UNMutableNotificationContent()
        content.title = "Time to take \(medicationName)"
        content.body = "It's time to take your medication!"
        content.sound = UNNotificationSound.default
        
        for i in weekdays {
            var dateInfo = DateComponents()
            
            let timeArray = time.components(separatedBy: ":")
            
            dateInfo.hour = Int(timeArray[0])
            dateInfo.minute = Int(timeArray[1])
            dateInfo.weekday = i
            dateInfo.timeZone = .current
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
            let request = UNNotificationRequest(identifier: "\(medicationName).\(i)", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error : Error?) in
                
                if let theError = error {
                    print(theError.localizedDescription)
                }
            }
        }
        
    }
}
