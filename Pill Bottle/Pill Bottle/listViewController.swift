//
//  listViewController.swift
//  Pill Bottle
//
//  Created by Christopher Warnock on 12/1/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class MyTableViewCell: UITableViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
}

class listViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    private var data: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Medication")
        
        //3
        do {
            data = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "reusableCell") as! MyTableViewCell
        
        let dataFromDatabase = data[indexPath.row]
        let label = dataFromDatabase.value(forKeyPath: "name") as! String
        let imageData = dataFromDatabase.value(forKeyPath: "picture") as! Data
        
        //cell.textLabel?.text = text
        cell.cellLabel?.text = label
        cell.cellImage?.image = UIImage(data: imageData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataFromDatabase = data[indexPath.row]
        let instructions = dataFromDatabase.value(forKeyPath: "directions") as! String
        let alert = UIAlertController(title: "Instructions", message: "\(instructions)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        //remove notification
        let dataFromDatabase = data[indexPath.row]
        let label = dataFromDatabase.value(forKeyPath: "name") as! String
        let monday = dataFromDatabase.value(forKeyPath: "mon") as! Bool
        let tuesday = dataFromDatabase.value(forKeyPath: "tues") as! Bool
        let wednesday = dataFromDatabase.value(forKeyPath: "wed") as! Bool
        let thursday = dataFromDatabase.value(forKeyPath: "thurs") as! Bool
        let friday = dataFromDatabase.value(forKeyPath: "fri") as! Bool
        let saturday = dataFromDatabase.value(forKeyPath: "sat") as! Bool
        let sunday = dataFromDatabase.value(forKeyPath: "sun") as! Bool
        
        if !sunday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).1"])
        }
        if !monday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).2"])
        }
        if !tuesday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).3"])
        }
        if !wednesday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).4"])
        }
        if !thursday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).5"])
        }
        if !friday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).6"])
        }
        if !saturday {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(label).7"])
        }
        
        //remove from database
        guard editingStyle == .delete else { return }
        let value = data[indexPath.row]
        
        data.remove(at: indexPath.row)
        
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(value)
        
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

}
