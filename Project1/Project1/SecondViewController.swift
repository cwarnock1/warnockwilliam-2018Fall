//
//  SecondViewController.swift
//  Project1
//
//  Created by Christopher Warnock on 10/22/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    
    private var datePicker: UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(SecondViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SecondViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        secondTextField.inputView = datePicker
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        secondTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        let array: [String] = [textField.text!, secondTextField.text!]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addedToList"), object: array)
        
        dismiss(animated: true, completion: nil)
    }

}
