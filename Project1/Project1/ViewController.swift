//
//  ViewController.swift
//  Project1
//
//  Created by Christopher Warnock on 10/22/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var measurments: [NSManagedObject] = []

    
    @objc func addToList(notification: Notification) {
        let recievedItem = notification.object as! [String]
        let number = Int(recievedItem[0])
        let date = recievedItem[1]
        
        self.save(value: number!, date: date)
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(addToList), name: Notification.Name(rawValue: "addedToList"), object: nil)
        
        title = "Weight Tracker"
        tableView.register(UITableViewCell.self,forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Measurments")
        
        //3
        do {
            measurments = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    


    @IBAction func addItem(_ sender: Any) {
        performSegue(withIdentifier: "mySegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return measurments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let measurment = measurments[indexPath.row]
        
        let value = measurment.value(forKeyPath: "weight") as! Int
        let date = measurment.value(forKeyPath: "date") as! String
        
        cell.textLabel?.text = "\(value) lbs"
        cell.detailTextLabel?.text = date
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let measurment = measurments[indexPath.row]
        
        measurments.remove(at: indexPath.row)
        
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(measurment)
        
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func save(value: Int, date: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Measurments", in: managedContext)!
        let measurment = NSManagedObject(entity: entity, insertInto: managedContext)
        
        measurment.setValue(value, forKeyPath: "weight")
        measurment.setValue(date, forKeyPath: "date")
        
        do {
            try managedContext.save()
            measurments.append(measurment)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    
}


