//
//  ThirdViewController.swift
//  homework1
//
//  Created by Christopher Warnock on 9/24/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import CoreData

class MyTableViewCell: UITableViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
}

class ThirdViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
//    private var data: [String] = []
    private var data: [NSManagedObject] = []
    
    @objc func addToList(notification: Notification) {
        let recievedItem = notification.object as! String
        
        self.save(value: recievedItem)
        self.tableView.reloadData()
    }
    
    func save(value: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Strings", in: managedContext)!
        let comment = NSManagedObject(entity: entity, insertInto: managedContext)
        
        comment.setValue(value, forKeyPath: "comment")
        
        do {
            try managedContext.save()
            data.append(comment)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addToList), name: Notification.Name(rawValue: "addedToList"), object: nil)

        // Do any additional setup after loading the view.
//        data.append("Table Rock")
//        data.append("Camel's Back")
//        data.append("Bob's Trail")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Strings")
        
        //3
        do {
            data = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "reusableCell") as! MyTableViewCell
        
        let text = data[indexPath.row]
        let value = text.value(forKeyPath: "comment") as! String
        
        //cell.textLabel?.text = text
        cell.cellLabel?.text = value
        cell.cellImage?.image = UIImage(named: "bob")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let tabBarViewController = tabBarController as! TabBarController
//        
//        switch indexPath.row {
//        case 0:
//            tabBarViewController.pictureName = "table"
//            tabBarViewController.textDescription = 1
//            tabBarViewController.selectedIndex = 1
//        case 1:
//            tabBarViewController.pictureName = "camel"
//            tabBarViewController.textDescription = 2
//            tabBarViewController.selectedIndex = 1
//        case 2:
//            tabBarViewController.pictureName = "bob"
//            tabBarViewController.textDescription = 3
//            tabBarViewController.selectedIndex = 1
//        default:
//            print("here4")
//        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let value = data[indexPath.row]
        
        data.remove(at: indexPath.row)
        
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(value)
        
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

}
