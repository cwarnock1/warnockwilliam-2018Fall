//
//  FourthViewController.swift
//  Project 2
//
//  Created by Christopher Warnock on 11/23/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func okButton(_ sender: Any) {
        let input : String = textField.text!
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addedToList"), object: input)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
