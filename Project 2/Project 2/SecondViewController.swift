//
//  SecondViewController.swift
//  homework1
//
//  Created by Christopher Warnock on 9/24/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var text: UILabel!
    
    var bool : Bool = true
    var pictureName: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let tabBarViewController = tabBarController as! TabBarController
        let pictureName:String = tabBarViewController.pictureName
        
        if pictureName == "" {
            image.image = UIImage(named: "smile")
        } else {
            image.image = UIImage(named: pictureName)
        }
        
        switch tabBarViewController.textDescription % 4{
        case 0:
            text.text = "Lorem ipsum dolor sit er elit lamet, consectetaur"
        case 1:
            text.text = "Table Rock Trail Head"
        case 2:
            text.text = "Camel's Back Park"
        case 3:
            text.text = "Bob's Trail"
        default:
            text.text = "Lorem ipsum dolor sit er elit lamet, consectetaur"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tabBarViewController = tabBarController as! TabBarController
        let pictureName:String = tabBarViewController.pictureName
        
        if pictureName == "" {
            image.image = UIImage(named: "smile")
        } else {
            image.image = UIImage(named: pictureName)
        }
        
        switch tabBarViewController.textDescription % 4{
        case 0:
            text.text = "Lorem ipsum dolor sit er elit lamet, consectetaur"
        case 1:
            text.text = "Table Rock Trail Head"
        case 2:
            text.text = "Camel's Back Park"
        case 3:
            text.text = "Bob's Trail"
        default:
            text.text = "Lorem ipsum dolor sit er elit lamet, consectetaur"
        }
    }
    
    @IBAction func rotateTapped(_ sender: Any) {
        let rotate = CGAffineTransform(rotationAngle: 180)
        let rotate2 = CGAffineTransform(rotationAngle: 0)
        if bool {
            UIView.animate(withDuration: 2.0, animations: { () -> Void in
                self.image.transform = rotate
            })
            bool = false
        }else {
            UIView.animate(withDuration: 2.0, animations: { () -> Void in
                self.image.transform = rotate2
            })
            bool = true
        }
    }
    


}

