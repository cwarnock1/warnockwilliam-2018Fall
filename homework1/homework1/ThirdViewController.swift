//
//  ThirdViewController.swift
//  homework1
//
//  Created by Christopher Warnock on 9/24/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    private var data: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        data.append("Table Rock")
        data.append("Camel's Back")
        data.append("Bob's Trail")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reusableCell")!
        
        let text = data[indexPath.row]
        
        cell.textLabel?.text = text
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tabBarViewController = tabBarController as! TabBarController
        
        switch indexPath.row {
        case 0:
            tabBarViewController.pictureName = "table"
            tabBarViewController.textDescription = 1
            tabBarViewController.selectedIndex = 1
        case 1:
            tabBarViewController.pictureName = "camel"
            tabBarViewController.textDescription = 2
            tabBarViewController.selectedIndex = 1
        case 2:
            tabBarViewController.pictureName = "bob"
            tabBarViewController.textDescription = 3
            tabBarViewController.selectedIndex = 1
        default:
            print("here4")
        }
        
    }

}
