//
//  FirstViewController.swift
//  homework1
//
//  Created by Christopher Warnock on 9/24/18.
//  Copyright © 2018 William Warnock. All rights reserved.
//

import UIKit
import MapKit

class FirstViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let initialLocation = CLLocation(latitude: 43.6148, longitude: -116.2042)
        centerMapOnLocation(location: initialLocation)
        addAnnotations()
        mapView.delegate = self
    }
    
    let regionRadius: CLLocationDistance = 10000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotations() {
        let tableRock = MKPointAnnotation()
        tableRock.title = "Table Rock Start"
        tableRock.coordinate = CLLocationCoordinate2D(latitude: 43.6036, longitude: -116.1622)
        
        let camelsBack = MKPointAnnotation()
        camelsBack.title = "Camel's Back Start"
        camelsBack.coordinate = CLLocationCoordinate2D(latitude: 43.6349, longitude: -116.2014)
        
        let bobsTrial = MKPointAnnotation()
        bobsTrial.title = "Bob's Trailhead"
        bobsTrial.coordinate = CLLocationCoordinate2D(latitude: 43.6585, longitude: -116.1743)
        
        mapView.addAnnotation(tableRock)
        mapView.addAnnotation(camelsBack)
        mapView.addAnnotation(bobsTrial)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let tabBarViewController = tabBarController as! TabBarController
        if let annotationTitle = view.annotation?.title
        {
            //print("User tapped on annotation with title: \(annotationTitle!)")
            switch annotationTitle {
            case "Table Rock Start":
                tabBarViewController.pictureName = "table"
                tabBarViewController.textDescription = 1
                tabBarViewController.selectedIndex = 1
            case "Camel's Back Start":
                tabBarViewController.pictureName = "camel"
                tabBarViewController.textDescription = 2
                tabBarViewController.selectedIndex = 1
            case "Bob's Trailhead":
                tabBarViewController.pictureName = "bob"
                tabBarViewController.textDescription = 3
                tabBarViewController.selectedIndex = 1
            default:
                print("here4")
            }
        }
        
    }

}

